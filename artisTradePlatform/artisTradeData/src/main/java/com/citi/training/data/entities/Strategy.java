package com.citi.training.data.entities;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Date;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


public class Strategy {
    private static final Logger log = LoggerFactory.getLogger(Strategy.class);
    
    private String stock;
    private Integer amount;
	private String strategy;
	private float cashbalance;
	private Integer lossmargin;
	private Integer profitmargin;
	
	
	public Strategy(String stock, Integer amount, String strategy, float cashbalance, Integer lossmargin,
			Integer profitmargin) {
		this.stock = stock;
		this.amount = amount;
		this.strategy = strategy;
		this.cashbalance = cashbalance;
		this.lossmargin = lossmargin;
		this.profitmargin = profitmargin;
	}
	
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getStrategy() {
		return strategy;
	}
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
	public float getCashbalance() {
		return cashbalance;
	}
	public void setCashbalance(float cashbalance) {
		this.cashbalance = cashbalance;
	}
	public Integer getLossmargin() {
		return lossmargin;
	}
	public void setLossmargin(Integer lossmargin) {
		this.lossmargin = lossmargin;
	}
	public Integer getProfitmargin() {
		return profitmargin;
	}
	public void setProfitmargin(Integer profitmargin) {
		this.profitmargin = profitmargin;
	}
	public static Logger getLog() {
		return log;
	}

}
