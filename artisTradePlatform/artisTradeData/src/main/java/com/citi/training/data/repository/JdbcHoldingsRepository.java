package com.citi.training.data.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.citi.training.data.entities.Holdings;

@Repository("JdbcHoldingsRepository")
public class JdbcHoldingsRepository implements HoldingsRepository {
	private static final Logger log = LoggerFactory.getLogger(JdbcTradeRepository.class);
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Value("${holdings.table.name:trades}")
    private String tableName = "holdings";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    
    public List<Holdings> getAllHoldings(){
    	log.debug("JdbcHoldingsRepo getAll");
    	List<Holdings> holdings = jdbcTemplate.query(selectAllSQL,
    			new BeanPropertyRowMapper<Holdings>(Holdings.class));
    	
    	log.debug("Query for all holdings returned list of size: " + holdings.size());
    	return holdings;
    }
}
