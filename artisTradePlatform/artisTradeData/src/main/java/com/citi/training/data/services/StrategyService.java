package com.citi.training.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.citi.training.data.entities.Strategy;
import com.citi.training.data.repository.StrategyRepository;

@Service
public class StrategyService {

    @Autowired
    private StrategyRepository strategyRepository;

    public Strategy saveStrategy(Strategy newStrategy) {
      return strategyRepository.saveStrategy(newStrategy);
  }

	public List<Strategy> list() {
		return strategyRepository.listStrategy();
	}

   
}

