package com.citi.training.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.entities.Holdings;
import com.citi.training.data.repository.HoldingsRepository;

@Service
public class HoldingsService {
	@Autowired
	HoldingsRepository holdingsRepository;
	
	public List<Holdings> getAllHoldings(){
		return holdingsRepository.getAllHoldings();
	}
}
