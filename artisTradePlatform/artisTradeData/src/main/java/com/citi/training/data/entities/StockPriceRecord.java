package com.citi.training.data.entities;

import java.sql.Timestamp;

public class StockPriceRecord {

    public StockPriceRecord() {
    }


    private Integer id;

    private String ticker;

    private Timestamp timeInspected;

    private Double price;

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof StockPriceRecord)) {
            return false;
        }
        StockPriceRecord s = (StockPriceRecord) o;
        return this.id == s.id;
    }

    public Integer getId() {
        return id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Timestamp getTimeInspected() {
        return timeInspected;
    }

    public void setTimeInspected(Timestamp timeInspected) {
        this.timeInspected = timeInspected;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
