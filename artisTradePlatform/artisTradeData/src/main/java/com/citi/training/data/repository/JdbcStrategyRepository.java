package com.citi.training.data.repository;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Strategy;
import com.citi.training.data.entities.Trade;

@Repository("JdbcStrategyRepository")
public class JdbcStrategyRepository implements StrategyRepository {

    private static final Logger log = LoggerFactory.getLogger(JdbcStrategyRepository.class);

    @Value("${trade.table.name:trades}")
    private String tableName = "strategies";

    private final String insertSQL = "INSERT INTO " + tableName + " (stock, price, size, lastStateChange, " +
                                     "tradeType, state) values (:stock, :price, :size, :lastStateChange, " +
                                     ":tradeType, :state)";

    private final String updateSQL = "UPDATE " + tableName + " SET stock=:stock, " +
                                     "price=:price, size=:size, lastStateChange=:lastStateChange, tradeType=:tradeType, state=:state " +
                                     "WHERE id=:id";

    private final String selectAll = "SELECT * FROM " + tableName ;
    private final String selectByIdSQL = "SELECT * FROM " + tableName + " WHERE id=?";
    private final String selectByStockSQL = "SELECT * FROM " + tableName + " WHERE stock=?";
    private final String selectByStateSQL = "SELECT * FROM " + tableName + " WHERE state=?";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    // returns the object given
    // if id < -1 then it will be inserted, otherwise updated
    public Trade saveTrade(Trade trade) {
        BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(trade);

        // use string value of tradeType and state enums
        namedParameters.registerSqlType("tradeType", Types.VARCHAR);
        namedParameters.registerSqlType("price", Types.FLOAT);
        namedParameters.registerSqlType("size", Types.INTEGER);
        namedParameters.registerSqlType("state", Types.VARCHAR);

        if (trade.getId() < 0) {
            // insert
            log.debug("Inserting trade: " + trade);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(insertSQL,namedParameters, keyHolder);
            trade.setId(keyHolder.getKey().intValue());
        } else {
            log.debug("Updating trade: " + trade);
            namedParameterJdbcTemplate.update(updateSQL, namedParameters);
        }
        log.info("JdbcRepo returning trade: " + trade);
        return trade;
    }



    public List<Trade> getTradesByStock(String stock) {
        log.debug("JdbcTradeRepo getByStock: " + stock);
        List<Trade> trades = jdbcTemplate.query(selectByStockSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class),
                                                stock);

        log.debug("Query for stock <" + stock + "> returned list of size: " + trades.size());
        return trades;
    }

    public List<Trade> getTradesByState(Trade.TradeState state) {
        log.debug("JdbcTradeRepo getByState: " + state);
        List<Trade> trades = jdbcTemplate.query(selectByStateSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class),
                                                state.toString());

        log.debug("Query for state <" + state + "> returned list of size: " + trades.size());
        return trades;
    }

    public List<Trade> getAllTrades() {
        log.debug("JdbcTradeRepo getAll");
        List<Trade> trades = jdbcTemplate.query(selectAllSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class));

        log.debug("Query for all returned list of size: " + trades.size());
        return trades;
    }

	@Override
	public Strategy saveStrategy(Strategy Strategy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Strategy> listStrategy() {
        log.debug("JdbcTradeRepo get all strategies");
        List<Strategy> strategies = jdbcTemplate.query(selectAll,
                                                new BeanPropertyRowMapper<Strategy>(Strategy.class));

        return strategies;
		}
	}

