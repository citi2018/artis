package com.citi.training.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// This application is here to create a spring context for tests
@SpringBootApplication()
public class TestApplication {

        public static void main(String[] args) {
                SpringApplication.run(TestApplication.class, args);
        }
}