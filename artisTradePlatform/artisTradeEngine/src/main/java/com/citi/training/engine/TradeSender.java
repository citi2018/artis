package com.citi.training.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class TradeSender {

	@Autowired
	private JmsTemplate jmsTemplate;

	public void sendSimple(String trade)
	{
		jmsTemplate.convertAndSend("OrderBroker", trade,
			message -> {
				message.setStringProperty("Operation", "Update");
				message.setJMSCorrelationID("Cathal");
				return message;
			});
	}
}
