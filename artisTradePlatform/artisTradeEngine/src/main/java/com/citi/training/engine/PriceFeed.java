package com.citi.training.engine;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

public class PriceFeed {
	public static void main(String [] args) {
		liveFeed("amzn");
	}
	static private ArrayList<String> onlineFeed = new ArrayList<String>();
	public static double liveFeed(String stockName){
		RestTemplate restTemplate = new RestTemplate();
		String url  = "http://feed.conygre.com:8080/MockYahoo/quotes.csv?s="
				+ stockName + "&f=p0";
		ResponseEntity<String> response
		  = restTemplate.getForEntity(url, String.class);
		onlineFeed.add(response.getBody());
		int feedLength = onlineFeed.size()-1;
		double price = Double.parseDouble(onlineFeed.get(feedLength));
		System.out.println(price);
		return price;
			}//liveFeed
		}//PriceFeed

