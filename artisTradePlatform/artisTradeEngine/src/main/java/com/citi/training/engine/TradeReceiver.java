package com.citi.training.engine;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class TradeReceiver {

	
	@JmsListener(destination="OrderBroker_Reply")
	public void receive(String message) {
		System.out.println("RECEIVED " +message);
	}
}
