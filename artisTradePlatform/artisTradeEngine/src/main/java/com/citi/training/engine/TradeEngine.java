package com.citi.training.engine;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.Trade;
import com.citi.training.data.services.TradeService;

@Component
public class TradeEngine {

    private static final Logger log = LoggerFactory.getLogger(TradeEngine.class);
    
    @Autowired
    private TradeService tradeService;

    @Autowired
    private TradeSender ts;
    
    @Scheduled(fixedRate = 20000)
    public void sendTrades() {
        List<Trade> initTrades = tradeService.getTradesByState(Trade.TradeState.INIT);

        log.debug("Processing " + initTrades.size() + " new trades");
        for(Trade trade : initTrades) {
        	// call the method with the algorithms to 
        	String message = Trade.toXml(trade);
        	System.out.println(message);
        	ts.sendSimple(message);
            trade.stateChange(Trade.TradeState.WAITING_FOR_REPLY);
            tradeService.saveTrade(trade);
            
        }
    }
}
