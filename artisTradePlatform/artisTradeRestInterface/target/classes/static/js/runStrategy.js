$(document).ready(startup);

function startup() {
	getCustomerControllerStatus();
	// hookupevents
	$("#submit").click(save);
}

function save() {

	var stock = $("#txtStockName").val();
	var amount = $("#txtAmount").val();
	var strategy = $("#txtStrategy").val();
	var timeFrame = $("#txtTimeFrame").val();
	var lossmargin = $("#txtLoss").val();
	var profitmargin = $("#txtProfit").val();

	var params = {
		stock:stock,
		price:100,
		amount:amount,
		strategy:strategy,
		timeFrame:timeFrame,
		profitmargin:profitmargin, 
	};
	
	var paramsJson = JSON.stringify(params);

	$.ajax({
		url : "/strategy/save",
		data : paramsJson,
		dataType : "json",
		type : "POST",
		cache : false,
		async : false,
		contentType: 'application/json; charset=utf-8',
		success : function(response) {
			console.log("saved")
		},
		error : function(err) {
			alert("Error: " + err.responseText)
		}
	});
}
