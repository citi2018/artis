#!/bin/bash

# Variables from the environment, if they are not set then use these values
if [[ -z DBSERVER ]]
then
  DBSERVER=localhost
fi
if [[ -z DBNAME ]]
then
  DNAME=myapp
fi
if [[ -z DBUSER ]]
then
  DBUSER=mydbusername
fi
if [[ -z DBPASS ]]
then
  DBPASS=mydbuserpassword
fi

# Create the properties file
cat >application.properties <<_END_
database.url=jdbc:mysql://${DBSERVER}:3306/${DBNAME}
database.user=${DBUSER}
database.password=${DBPASS}

_END_

# Start the application

java -jar myapp.jar
